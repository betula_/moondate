const getMoonData = require('./get-moon-data');

module.exports = (number) => getMoonData()[ number - 1 ];
