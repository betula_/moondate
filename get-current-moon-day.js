const lunarDays = require('lunardays')
const moment = require('moment')

module.exports = () => {
  // create some date
  const date = moment()
  // MSK
  const latitude = 56
  const longitude = 38
  // get all lunar days for date
  const days = lunarDays(date, latitude, longitude)

  const { number } = days
    .filter(({ start, end }) => date.isBetween(start, end))
  [0];

  return number;
}
