const data = require('./data');

module.exports = () =>
  data
    .trim()
    .split('->>')
    .map((s) => s.trim())
    .filter((s) => s.length)
    .map((s) => {
      const lines = s
        .split(/\n/)
        .map((s) => s.trim())
        .filter((s) => s.length);

      return {
        title: lines[0],
        content: lines.slice(1)
      }
    });
;
