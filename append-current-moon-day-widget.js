
const getCurrentMoonDayData = require('./get-current-moon-day-data');

module.exports = (node) => {
  const data = getCurrentMoonDayData();

  node.innerHTML = `
          <h1>${data.title}</h1>
          <pre>${data.content.join('\n')}</pre>
        `;
};
