const appendCurrentMoonDayWidget = require('./append-current-moon-day-widget');

((window) => {
  (window || {}).appendCurrentMoonDayWidget = appendCurrentMoonDayWidget;
})(window);
