const getCurrentMoonDay = require('./get-current-moon-day');
const getMoonDayData = require('./get-moon-day-data');

module.exports = () => getMoonDayData(getCurrentMoonDay());
